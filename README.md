# Distribution Gluer
**Program description**

Program to connect/reweight distributions generated at different pseudo-temperatures.
Usually used for large-deviation simulations.

**Disclaimer**

This software is provided without warranty of any kind.
(If you find any bugs or have suggestions, please consider to create an issue in GitLab.)

**How to run the script?**

To run the python script, a recent version of Anaconda (https://www.anaconda.com/products/individual) is recommended.
Otherwise, the following libraries must be installed in your local python3 environment:
- numpy
- gmpy2
- matplotlib
- scipy

As a starting point, run the script with the "--help" option to get an overview. 
For example, type into your command line: python3 distribution_gluer.py --help

**Functionalities**

The "--help" option should be the go to point for feature information and on what the individual options do.
Mainly, more reference is given here on the implementation details and some general hints.

The core functionality of the program is the "gluing"-algorithm (selected with the "--glue" option), of which there are two: 
- Least-Squares (LSQRS): The square deviation in the overlapping region between two histograms/distributions is minimized.
This is done analytically and therefore very fast (even for large amounts of data), but "glueing" performance generally depends 
on the quality/statistics of the histograms itself (consider using the "--std_multiple" option to "cut off" histogram 
bins far away from the mean).
The method also works when the distributions are independent of any pseudo-temperature (for example in case of Wang-Landau sampling).
- Ferrenberg and Swendsen Method (FS): This method is described in the paper under the following link:
https://doi.org/10.1103/PhysRevLett.63.1195 .
This method is usually more "robust" with respect to the histogram statistics.
Please note: A numerical solver is used here, which, depending on the data and number of bins, 
does not guarantee to converge and may take some time to process.

The program uses gmpy2, a python implementation of the GNU MPFR Library (https://www.mpfr.org/) to deal with very large/small numbers.

To check whether the final curve is properly connected, the "--plot" option will conveniently plot the result immediately.

Consider using the "--binning_file" option, in case histograms are unsatisfactory.

The normalization is such that the output file contains a probability density.

**Known Issues/TODOs**

For the LSQRS method, the error propagation neglects correlations.

