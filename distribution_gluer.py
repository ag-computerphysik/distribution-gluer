#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 20 12:50:33 2021

@author: peter
"""

import numpy as np
import gmpy2
import argparse
import sys
import matplotlib.pyplot as plt
from scipy.optimize import root as opt_root

#%% Function definitions
mpfr_exp = np.vectorize(gmpy2.exp)
mpfr_isnan = np.vectorize(gmpy2.is_nan)
mpfr_log = np.vectorize(gmpy2.log)
mpfr_sqrt = np.vectorize(gmpy2.sqrt)

#%% Define and process command line parameters.
parser = argparse.ArgumentParser(
    description="A program to create and connect histograms/distributions.")
parser.add_argument(
    "-f", "--files", nargs='+', required=True, 
    help=("Space separated list of files containing the data. "
          "When raw data is provided (see --create option), only one colum is "
          "expected. The data should be uncorrelated. "
          "When histograms are provided, they all must have the same binning! "
          "Expected file format: First column: bin center; Second column: "
          "bin width, Third column: bin value (usually bin counts); "
          "Fourth column: bin value standard deviation (LSQRS method only). "
          "If no fourth column is provided a standard deviation will be "
          "calculated internally."))
parser.add_argument(
    "-i", "--ignore_error", action="store_true", default=False,
    help="If set, the standard deviation provided in FILES will be ignored and "
         "and no estimation is done internally. (LSQRS only)")
parser.add_argument(
    "-o", "--output", required=True,
    help=("Name of output file to which the final probability density is written. "
          "File format: First column: bin center; Second column: probability "
          "density; Third column: standard deviation."))
parser.add_argument(
    "-p", "--pseudo", nargs='*', type=float, default=[],
    help=("Space separated list of pseudo temperatures used to rescale the "
          "histograms. When provided each histogram is reweighted with "
          "exp((bin value)/(pseudo temperature)). The pseudo temperatures are "
          "applied in file list order (see -f option). "
          "'inf' is a valid argument in case of simple sampling."
          "When not provided, it is assumed that FILES contain data "
          "that is already reweighted."))
parser.add_argument(
    "-g", "--glue", choices=["LSQRS", "FS"], default="LSQRS",
    help="Gluing method to connect the histograms. Default is LSQRS. "
         "FS method requires pseudo temperatures (see --pseudo option).")
parser.add_argument(
    "-b", "--binning_seperator", nargs='*', type=float, default=[],
    help=("Will determine an independent binning in the intervalls seperated "
          "by the provided points. Only used in conjunction with the "
          "'--create' option. Ignored when '--binning_file' option is set."))
parser.add_argument(
    "-B", "--binning_file", default="",
    help=("When given, will load bin edges from specified file. Expected file "
          "format: one column. Superseeds all other binning options. "
          "Only used in conjunction with the '--create' option."))
parser.add_argument(
    "-O","--overlap", type=int, default=2,
    help=("Minimum number of common points along the x-Axis between histograms " 
          "that are required for them to be connected directly. Default is 2. "
          "(Only relevant for LSQRS)"))
parser.add_argument(
    "-s", "--std_multiple", type=float, default=np.inf,
    help=("Histograms are cut off, when values are more than STD_MULTIPLE "
          "times the standard deviation away from the expectation value."))
parser.add_argument(
    "-c", "--create", action="store_true", default=False, 
    help=("Programm will create histograms/probability densities by itself. "
          "The provided files are assumed to contain the data to be "
          "histogrammed."))
parser.add_argument("-P", "--plot", action="store_true", default=False,
    help=("When set, will display a plot of the original and reweighted "
          "histrograms using matplotlib."))
parser.add_argument(
    "-l", "--no_log_plot", action="store_true", default=False,
    help=("When set, the plot will use linear axis scaling"))
args = parser.parse_args()
if args.glue == "FS" and len(args.pseudo) == 0:
    print("error: the Ferrenberg-Swendsen (FS) method requires pseudo "
          "temperatures (see option --pseudo).")
    sys.exit()

#%% Load data
data = [np.loadtxt(f) for f in args.files]
if len(args.pseudo) != 0:
    large_dev_temperatures = np.array(args.pseudo)
else:
    large_dev_temperatures = np.inf * np.ones(len(args.files))

#%% Prepare data for the connection/merging procedure
# Depending on the procedures, some of these are required variables:
histograms_counts = []
histograms = [] # Here this ist the probability (not probability density)
std_histograms = []
bin_centers = None
bin_widths = None

bin_edges = None
if args.create: # Calculate histograms
    if args.binning_file: # Load provided binning
        bin_edges = np.sort(np.loadtxt(args.binning_file))
    elif args.binning_seperator:
        binning_separator_points = (
            np.sort(np.array(args.binning_seperator + 
                             [np.min(np.concatenate(data)), 
                              np.max(np.concatenate(data))])))
        bin_edges = [np.histogram_bin_edges(
            np.concatenate(data), bins='auto', 
            range=(binning_separator_points[i], binning_separator_points[i+1])) 
            for i in range(len(binning_separator_points)-1)]
        bin_edges = np.concatenate([b[:-1] for b in bin_edges] + 
                                   [np.array([bin_edges[-1][-1]])])
    else:
        bin_edges = np.histogram_bin_edges(np.concatenate(data), bins='auto')
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2.0
    bin_widths = np.abs(bin_edges[1:] - bin_edges[:-1])
    histogram_counts = [np.histogram(d, bins=bin_edges)[0] for d in data]
    histograms = [c / d.size for c, d in zip(histogram_counts, data)]
    std_histograms = [np.sqrt(h * (1 - h) / d.size) 
                      for h, d in zip(histograms, data)]
else: # Load histograms
    bin_centers = data[0][:, 0] 
    bin_widths = data[0][:,1]
    histogram_counts = [d[:, 2] for d in data]
    histograms = [c / np.sum(c) for c in histogram_counts]
    std_histograms = [d[:, 3] / np.sum(d[:,2]) if d.shape[1] == 4 
                      else np.sqrt(h * (1 - h) / np.sum(d[:,2])) 
                      for h, d in zip(histograms, data)]
    
for std, h in zip(std_histograms, histograms):
    std[h == 0] = np.inf
if not np.isinf(args.std_multiple): # Cut off outliers
    mean_values = [np.average(bin_centers, weights=h) for h in histograms]
    std_values = [np.sqrt(np.average((bin_centers-m)**2, weights=h))
                  for h, m in zip(histograms, mean_values)] 
    for c, std, h, mean, sig in zip(histogram_counts, std_histograms, 
                                    histograms, mean_values, std_values):
        cond = np.logical_and(bin_centers > mean - args.std_multiple * sig, 
                              bin_centers < mean + args.std_multiple * sig)
        h[np.logical_not(cond)] = 0
        c[np.logical_not(cond)] = 0
        std[h == 0] = np.inf
    
#%% Apply connecting/merging procedure
# From here on the mpfr data type is used to handle extreme numbers
if args.glue == "LSQRS":
    # Rescale histograms via pseudo temperature
    reweighted_histograms = (
        [mpfr_exp(gmpy2.mpfr(1) * bin_centers / theta) * h 
         for theta, h in zip(large_dev_temperatures, histograms)])
    std_reweighted_histograms = (
        [mpfr_exp(gmpy2.mpfr(1) * bin_centers / theta) * std 
         for theta, std in zip(large_dev_temperatures, std_histograms)])
    
    # Find scaling coeffincients between histograms
    scaling_coefficients = (
        gmpy2.mpfr(0) * np.zeros((len(reweighted_histograms), 
                                  len(reweighted_histograms))))
    scaling_quality = gmpy2.mpfr(0) * np.zeros((len(reweighted_histograms), 
                                                len(reweighted_histograms)))
    for i in range(0,len(reweighted_histograms)):
        for j in range(0,len(reweighted_histograms)):
            if (i == j):
                continue
            cond1 = np.logical_and(reweighted_histograms[i] != 0, 
                                   reweighted_histograms[j] != 0)
            cond2 = np.logical_and(mpfr_isnan(reweighted_histograms[i]) == False, 
                                   mpfr_isnan(reweighted_histograms[j]) == False)
            cond = np.logical_and(cond1, cond2)
            if np.sum(cond) < args.overlap:
                continue
            if args.ignore_error:
                weight = np.ones(std_histograms[i][cond].shape)
            else:
                weight = std_histograms[i][cond]**2 + std_histograms[j][cond]**2
            scaling_coefficients[i][j] = (
                np.sum(reweighted_histograms[j][cond] *
                       reweighted_histograms[i][cond] / weight))
            scaling_coefficients[i][j] /= (
                np.sum(reweighted_histograms[i][cond] *
                       reweighted_histograms[i][cond] / weight))
            scaling_quality[i][j] = 1.0 / np.sum(weight)
    
    # Multiply histograms with scaling coefficients in a chain like manner, 
    # beginning from the histogram at max. pseude temperature.
    # TODO: Improve "chain"-selection by incorporating the scaling_quality
    next_index = np.argmax(large_dev_temperatures)
    unvisited = [*range(0, next_index), 
                 *range(next_index + 1, len(reweighted_histograms))]
    previous = [next_index]
    while len(previous) > 0 and len(unvisited) > 0:
        next_index=previous.pop()
        for i, s in enumerate(scaling_coefficients[next_index]):
            if i in unvisited and s != 0:
                unvisited.remove(i)
                reweighted_histograms[i] /= s
                std_reweighted_histograms[i] /= s
                scaling_coefficients[i] *= s
                previous.append(i)
    for i in unvisited: # ignore histograms that are unconnected to the rest
        reweighted_histograms[i] = (
            gmpy2.mpfr(0) * np.zeros(reweighted_histograms[i].shape))
        std_reweighted_histograms[i] = (
            gmpy2.mpfr(np.inf) * np.ones(std_reweighted_histograms[i].shape))
     
    # Combine all histogram using a weighted average
    # TODO: More sophisticated error estimation using bootstrap resampling
    # TODO: Also add an option for this simpler method.
    total_histogram = gmpy2.mpfr(0) * np.zeros(bin_centers.shape)
    std_total_histogram = gmpy2.mpfr(0) * np.zeros(bin_centers.shape)
    if args.ignore_error:
       std_total_histogram = 0 * std_total_histogram
       for h in reweighted_histograms:
           total_histogram += h
       total_histogram /= np.sum(total_histogram)
    else:
        for std, h in zip(std_reweighted_histograms, reweighted_histograms):
            total_histogram += h / std**2
            std_total_histogram += 1.0 / std**2
        std_total_histogram[total_histogram == 0] = gmpy2.mpfr(np.inf)
        total_histogram /= std_total_histogram
        std_total_histogram = 1.0 / std_total_histogram
        std_total_histogram = mpfr_sqrt(std_total_histogram) / np.sum(total_histogram)
        total_histogram /= np.sum(total_histogram)

elif args.glue == "FS":
    bin_counts = np.zeros(bin_centers.shape)
    free_energies = np.ones(large_dev_temperatures.shape)
    weights = []
    for c in histogram_counts:
        bin_counts += c
    for c, ldt in zip(histogram_counts, large_dev_temperatures):
        weights.append(np.sum(c) * 
                       mpfr_exp(-gmpy2.mpfr(1) * bin_centers / ldt))
    def optimize_func(fe):
        result = gmpy2.mpfr(0) * np.zeros(fe.shape)
        denominator = gmpy2.mpfr(0) * np.zeros(bin_centers.shape)
        for w, f in zip(weights, fe):
            denominator += w * gmpy2.exp(f)
        density = bin_counts / denominator
        for i, ldt in enumerate(large_dev_temperatures):
              result[i]= -mpfr_log(np.sum(density * 
                                mpfr_exp(-gmpy2.mpfr(1) * 
                                          bin_centers / ldt))) - fe[i]
        return result.astype(np.float64)
    optimization_result = opt_root(optimize_func, free_energies, 
                                   method="df-sane")
    free_energies = optimization_result.x
    print(optimization_result.message)
    denominator = gmpy2.mpfr(0) * np.zeros(bin_centers.shape)
    for w, f in zip(weights, free_energies):
        denominator += w * gmpy2.exp(f)
    density = bin_counts / denominator
    
    #TODO: Add arbitrary target temperature
    total_histogram = density
    std_total_histogram = gmpy2.mpfr(0) * np.zeros(bin_centers.shape)
    cond = total_histogram != 0
    std_total_histogram[cond] = total_histogram[cond] / np.sqrt(bin_counts)[cond]
    std_total_histogram[total_histogram == 0] = gmpy2.mpfr(0)
    std_total_histogram /= np.sum(total_histogram)
    total_histogram /= np.sum(total_histogram)

# Transform to a probability density by dividing with the bin width
total_histogram /= bin_widths
std_total_histogram /= bin_widths

#%% Save data
# TODO: check gmpy2.mpfr conformant output 
np.savetxt(args.output, 
           np.array([bin_centers, total_histogram, std_total_histogram]).T,
           header="bin_center probability_density standard_deviation")

#%% Plot rescaled, merged/connected distribution
# Please note: The plot functions do not work with the mpfr data type.
if args.plot:
    plt.figure()
    plt.subplot(2, 1, 1)
    index_order = np.argsort(large_dev_temperatures)
    if args.glue == "LSQRS":
        for i in index_order:
            cond = reweighted_histograms[i] != 0
            if args.ignore_error:
                error_bar = np.zeros(std_reweighted_histograms[i][cond].shape)
            else:
                error_bar = std_reweighted_histograms[i][cond].astype(np.float128)
            plt.errorbar(bin_centers[cond], 
                         reweighted_histograms[i][cond].astype(np.float128) 
                         / bin_widths[cond], 
                         yerr=error_bar 
                         / bin_widths[cond],
                         fmt="x", linewidth=2, capsize=6, 
                         label=str(large_dev_temperatures[i]))
        if len(args.pseudo) != 0:
            plt.legend(ncol=3, title="Pseudo-Temperatures")
        plt.grid(True)
        if not args.no_log_plot:
            plt.yscale("log")
        plt.xlabel("Bin Value")
        plt.ylabel("Rescaled Probability")
    elif args.glue == "FS":
        cond = total_histogram != 0
        plt.errorbar(bin_centers[cond], 
                     total_histogram[cond].astype(np.float128), 
                     yerr=std_total_histogram[cond].astype(np.float128), 
                     fmt="x", linewidth=2, 
                     capsize=6)
        plt.grid(True)
        if not args.no_log_plot:
            plt.yscale("log")
        plt.xlabel("Bin Value")
        plt.ylabel("Final Probability")
    
    plt.subplot(2, 1, 2)
    for i in index_order:
        cond = histograms[i] != 0
        if args.ignore_error:
            error_bar = np.zeros(std_histograms[i][cond] .shape)
        else:
            error_bar = std_histograms[i][cond] / bin_widths[cond]
        plt.errorbar(bin_centers[cond], histograms[i][cond] / bin_widths[cond], 
                     yerr=error_bar,
                     fmt="x", linewidth=2, capsize=6,
                     label=str(large_dev_temperatures[i]))
    if args.glue == "FS":
        plt.legend(ncol = 3, title="Pseudo-Temperatures")
    plt.grid(True)
    if not args.no_log_plot:
        plt.yscale("log")
    plt.xlabel("Bin Value")
    plt.ylabel("Original Probability")
    
    plt.show()









